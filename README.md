facetimehd
==========

Linux driver for the Facetime HD (Broadcom 1570) PCIe webcam
found in recent Macbooks.

This driver is experimental. Use at your own risk.

See the [Wiki][wiki] for more information:

## installation
```
$ git clone https://github.com/thelastinuit/bcwc_pcie.git
$ cd bcwc_pcie/firmware
$ make
$ sudo make install
$ cd ..
$ make
$ sudo make install
$ sudo depmod
$ sudo modprobe -r bdc_pci
$ sudo modprobe facetimehd
$ echo 'facetime' >> /etc/modules
```
